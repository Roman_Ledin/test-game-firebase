const admin = require("firebase-admin");


const createPosition = async (gameName) => {
  try {
    const db = admin.firestore();
    let needPos = 1;
    const {collectionName} = require("../constants/collectionName");
    const statisticRef = db
        .collection(`${collectionName.playerStatistic}_${gameName}`)
        .orderBy("position", "desc");
    const snap = await statisticRef.limit(1).get();
    if (snap.docs.length) {
      snap.forEach((element) => {
        needPos = element.data().position + 1;
      });
    }
    return needPos;
  } catch (err) {
    return {
      response: "error",
      message: err.message,
    };
  }
};

module.exports = {
  createPosition,
};
