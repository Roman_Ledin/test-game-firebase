const setGame = (gameId) => {
  switch (gameId) {
    case "domino":
      return "domino";
    case "cards":
      return "cards";
    case "chess":
      return "chess";
    default:
      return null;
  }
};

module.exports = {
  setGame,
};
