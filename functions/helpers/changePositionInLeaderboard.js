const admin = require("firebase-admin");
const {batchIt} = require("./batchFunc");


const changePosition = async (oldScore, newScore, gameName, userId) => {
  try {
    const db = admin.firestore();
    const {collectionName} = require("../constants/collectionName");
    let isSwitch = false;

    const statisticRef = db
        .collection(`${collectionName.playerStatistic}_${gameName}`)
        .orderBy("score", "desc");
    const snap = await statisticRef.startAt(newScore).endAt(oldScore)
        .get();

    if (snap.docs.length) {
      const arr = snap.docs.map((item) => {
        const gameStats = item.data();
        return gameStats;
      });

      const arrForBatch = [];
      let minPosition = arr[0].position;

      snap.docs.forEach((item) => {
        const gameStats = item.data();
        if (gameStats.position <= minPosition) {
          minPosition = gameStats.position;
        }
        if (gameStats.userId === userId) {
          isSwitch = true;
        }
        if (isSwitch === false) {
          if (arrForBatch.length < 499) {
            gameStats.position += 1;
            arrForBatch.push(gameStats);
          } else {
            batchIt(arrForBatch, gameName);
            arrForBatch.length = 0;
          }
        }
      },
      );
      return minPosition;
    } else {
      return 1;
    }
  } catch (err) {
    return {
      response: "error",
      message: err.message,
    };
  }
};

module.exports = {
  changePosition,
};
