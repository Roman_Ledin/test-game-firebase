const admin = require("firebase-admin");
const {collectionName} = require("../constants/collectionName");

const botAdder = (arr1, arr2, arr3, arr4, gameName) => {
  const db = admin.firestore();
  const batch = db.batch();

  arr2.forEach((element) => {
    const statisticRef = db
        .collection(`${collectionName.playerStatistic}_${gameName}`)
        .doc(element.playerId);
    batch.set(statisticRef, element);
  });
  arr1.forEach((element) => {
    const globalUserSave =
    db.collection(collectionName.globalUsers)
        .doc(element.deviceId);
    batch.set(globalUserSave, element);
  });
  arr3.forEach((element) => {
    const gameClientSave =
    db.collection(collectionName.gameClients)
        .doc(element.playerId);
    batch.set(gameClientSave, element);
  });
  arr4.forEach((element) => {
    const refGameCollection = db
        .collection(gameName).doc(`${gameName}_${element.playerId}`);
    batch.set(refGameCollection, element);
  });

  batch.commit();
};

module.exports = {
  botAdder,
};
