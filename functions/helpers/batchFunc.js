const admin = require("firebase-admin");
const {collectionName} = require("../constants/collectionName");

const batchIt = async (arrForBatch, gameName) => {
  const db = admin.firestore();
  const batch = db.batch();
  for (let i = 0; i < 499; i += 1) {
    arrForBatch.forEach((item) => {
      const statisticRef = db
          .collection(`${collectionName.playerStatistic}_${gameName}`)
          .doc(item.playerId);
      batch.set(statisticRef, item);
    });
    await batch.commit();
  }
};

module.exports = {
  batchIt,
};
