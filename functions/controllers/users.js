const functions = require("firebase-functions");
const users = require("../models/users.js");


exports.createUser = functions
    .https
    .onRequest(async (req, res) => {
      const {body} = req;
      try {
        const writeResult = await users.createUser(body);
        if (writeResult.response === "error") {
          return res.status(500).send(writeResult);
        }
        return res.status(200).send(writeResult);
      } catch (err) {
        functions.logger.log(err, "createUser err catch");
        return res.status(500).send({response: "error", message: err.message});
      }
    });

exports.getUser = functions
    .https
    .onRequest(async (req, res) => {
      const {
        query,
        headers,
      } = req;
      try {
        const result = await users.getUser(query.userId, headers.usertokenjwt);
        if (result.response === "error") {
          return res.status(400).send(result);
        }
        return res.status(200).send({response: "ok", data: result.user});
      } catch (err) {
        return res.status(500).send({response: "error", message: err.message});
      }
    });

exports.updateStats = functions
    .https
    .onRequest(async (req, res) => {
      const {
        query,
        headers,
        body,
      } = req;
      try {
        const result = await users
            .updateStats(body.data, query.userId, headers.usertokenjwt);
        if (result.response === "error") {
          return res.status(400).send(result);
        }
        return res.status(200).send({response: "ok", data: result.user});
      } catch (err) {
        return res.status(500).send({response: "error", message: err.message});
      }
    });

exports.addBots = functions
    .https
    .onRequest(async (req, res) => {
      const {
        body,
      } = req;
      try {
        const result = await users
            .addBots(body.data);
        if (result.response === "error") {
          return res.status(400).send(result);
        }
        return res.status(200).send({response: "ok", data: result.user});
      } catch (err) {
        return res.status(500).send({response: "error", message: err.message});
      }
    });

exports.getLeaderboard = functions
    .https
    .onRequest(async (req, res) => {
      try {
        const result = await users
            .getLeaderboard(req);
        if (result.response === "error") {
          return res.status(400).send(result);
        }
        return res.status(200).send({response: "ok", result});
      } catch (err) {
        return res.status(500).send({response: "error", message: err.message});
      }
    });

