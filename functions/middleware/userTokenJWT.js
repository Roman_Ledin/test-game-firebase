
const jwt = require("jsonwebtoken");

const SECRET = "SuperSecretKeyVerySuper";

const createToken = (data) => {
  try {
    const deviceId = data.deviceId;
    const gameId = data.gameId;
    const playerId = data. playerId;
    return jwt.sign({deviceId, gameId, playerId}, SECRET, {algorithm: "HS256"});
  } catch (err) {
    return {response: "error", message: "token creating error"};
  }
};

const decipherToken = (userTokenJWT) => {
  try {
    const decoded = jwt.verify(userTokenJWT, SECRET, {algorithm: "HS256"});
    return decoded;
  } catch (err) {
    return {response: "error", message: "token deciphering error"};
  }
};

module.exports = {createToken, decipherToken};
