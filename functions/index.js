// const functions = require("firebase-functions");
const admin = require("firebase-admin");
const users = require("./controllers/users");

// const serviceAccount =
// require("./firebase-new-project-310914-2eed8fe20cb7.json");

admin.initializeApp(
    // {credential: admin.credential.cert(serviceAccount)},
);


exports.createUser = users.createUser;
exports.getUser = users.getUser;
exports.updateStats = users.updateStats;
exports.addBots = users.addBots;
exports.getLeaderboard = users.getLeaderboard;
