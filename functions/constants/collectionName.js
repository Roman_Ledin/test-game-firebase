const collectionName = {
  deviceId: "device_info",
  gameClients: "game_clients",
  globalUsers: "global_users",
  playerStatistic: "players_statistics",
};

module.exports = {
  collectionName,
};
