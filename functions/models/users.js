const admin = require("firebase-admin");
// const functions = require("firebase-functions");
const {v4: uuidv4} = require("uuid");
const moment = require("moment");
const geoip = require("geoip-lite");
const {setGame} = require("../helpers/choiceGame");
const {createToken, decipherToken} = require("../middleware/userTokenJWT");

const {collectionName} = require("../constants/collectionName");
const {scoreCounter} = require("../helpers/scoreCounter");
const {botAdder} = require("../helpers/botAdder");
const {createPosition} = require("../helpers/newUserToLeaderboard");
const {changePosition} = require("../helpers/changePositionInLeaderboard");

const createUser = async (req) => {
  try {
    const db = admin.firestore();
    const dateNow = moment.utc();
    const batch = await db.batch();
    const playerId = uuidv4();
    const {gameId, nickname, photoUrl, deviceId, clientVersion, clientIp} = req;

    // check gameId
    const checkGameId = gameId && setGame(gameId) ?
          gameId : null;
    if (!checkGameId) {
      return {response: "error", message: "invalid gameId"};
    }

    const gameName = setGame(gameId);
    const gameCollection = {
      nickname: nickname ? nickname : null,
      photoUrl: photoUrl ? photoUrl : null,
      playerId,
      balance: {cash: 0},
      lvl: 0,
    };
    const gameStatsDefault = {
      allRounds: 0,
      winRounds: 0,
      score: 0,
      duration: 0,
    };
    const gameClient = {
      gameId,
      clientVersion: clientVersion ? clientVersion : null,
      playerId,
      created: dateNow,
      updated: dateNow,
      isOnline: false,
    };

    const globalUsersInfo = await db.collection(collectionName.globalUsers)
        .doc(`${deviceId}`).get();
    const deviceUser = globalUsersInfo.data();
    if (!deviceUser) {
      const geoloc = clientIp ? geoip.lookup(clientIp) : [];
      const newUserId = uuidv4();
      const globalUser = {
        deviceId: deviceId,
        userId: newUserId,
        createdAt: dateNow,
        updatedAt: dateNow,
        geoloc,
      };

      // CREATE TOKEN
      const dataForJWT = {
        playerId: playerId,
        userId: newUserId,
        gameId: gameId,
      };
      const userTokenJWT = createToken(dataForJWT);
      if (!userTokenJWT) {
        return {response: "error", message: "error Token"};
      }

      // SAVE GLOBAL ACC
      const globalUserSave =
      db.collection(collectionName.globalUsers)
          .doc(deviceId);
      batch.set(globalUserSave, globalUser);

      // SAVE CLIENTSAPPS
      const gameClientSave =
      db.collection(collectionName.gameClients)
          .doc(playerId);
      batch.set(gameClientSave, {userId: newUserId,
        userTokenJWT,
        ...gameClient});

      // SAVE GAME
      gameCollection.userId = newUserId;
      const refGameCollection = db
          .collection(gameName).doc(`${gameName}_${playerId}`);
      batch.set(refGameCollection, gameCollection);

      const position = await createPosition(gameName);

      // SAVING GAMESTATS
      const statisticRef = db
          .collection(`${collectionName.playerStatistic}_${gameName}`)
          .doc(playerId);
      batch.set(statisticRef, {
        position,
        playerId,
        userId: newUserId,
        ...gameStatsDefault,
      });

      const result = await batch.commit();
      if (result) {
        return {
          response: "ok",
          message: result[0],
          userTokenJWT,
          userId: newUserId,
        };
      } else {
        return {
          response: "error",
          message: "error creating a new account",
        };
      }
    } else {
      const ClientsAppsRef = db.collection(collectionName.gameClients);
      const snapshot = await ClientsAppsRef
          .where("userId", "==", deviceUser.userId).get();
      let checkExisting = true;
      snapshot.forEach((doc) => {
        if (doc.data().gameId === gameId) {
          return checkExisting = false;
        }
      });

      // CREATE NEW TOKEN
      const dataForJWT = {
        playerId: playerId,
        userId: deviceUser.userId,
        gameId: gameId,
      };
      const userTokenJWT = createToken(dataForJWT);
      if (!userTokenJWT) {
        return {response: "error", message: "error Token"};
      }

      //  SAVE CLIENTSAPPS
      const gameClientSave =
        db.collection(collectionName.gameClients)
            .doc(playerId);
      gameClient.userId = deviceUser.userId;
      gameClient.userTokenJWT = userTokenJWT;

      // SAVE GAME
      gameCollection.userId = deviceUser.userId;
      const refGameCollection = db
          .collection(gameName).doc(`${gameName}_${playerId}`);

      // SAVING GAMESTATS
      const statisticRef = db
          .collection(`${collectionName.playerStatistic}_${gameName}`)
          .doc(playerId);
      const position = await createPosition(gameName);

      if (checkExisting) {
        batch.set(statisticRef, {
          position,
          playerId,
          userId: deviceUser.userId,
          ...gameStatsDefault,
        });
        batch.set(refGameCollection, gameCollection);
        batch.set(gameClientSave, gameClient);
        const result = await batch.commit();
        if (result) {
          return {
            response: "ok",
            message: result[0],
            userTokenJWT,
            userId: deviceUser.userId,
          };
        }
      } else {
        return {
          response: "ok",
          message: `you already in ${gameName}`,
        };
      }
    }
  } catch (err) {
    return {
      response: "error",
      message: err.message,
    };
  }
};

const getUser = async (userId, userTokenJWT) => {
  const db = admin.firestore();
  try {
    const decoded = decipherToken(userTokenJWT);
    const gameId = decoded.gameId && setGame(decoded.gameId) ?
    decoded.gameId : null;
    const getGame = await db.collection(gameId)
        .doc(`${gameId}_${decoded.playerId}`).get();
    const gameData = getGame.data();

    if (decoded.playerId !== gameData.playerId || gameData.userId !== userId) {
      return {
        response: "error",
        message: "wrong JWT/PlayerId/UserId",
      };
    } else {
      return {
        user: getGame,
      };
    }
  } catch (err) {
    return {
      response: "error",
      message: err.message,
    };
  }
};

const updateStats = async (data, userId, userTokenJWT) => {
  const db = admin.firestore();
  const batch = await db.batch();

  try {
    const decoded = decipherToken(userTokenJWT);
    const gameId = decoded.gameId && setGame(decoded.gameId) ?
      decoded.gameId : null;

    const getGameStats = await
    db.collection(`${collectionName.playerStatistic}_${gameId}`)
        .doc(decoded.playerId).get();

    const ref = db.collection(`${collectionName.playerStatistic}_${gameId}`)
        .doc(decoded.playerId);
    const gameDataStats = getGameStats.data();

    if (decoded.playerId !== gameDataStats.playerId ||
    gameDataStats.userId !== userId) {
      return {
        response: "error",
        message: "wrong JWT/PlayerId/UserId",
      };
    } else {
      const score = scoreCounter(gameId, data.gameType,
          data.winRounds, data.rounds);

      const oldScore = gameDataStats.score;
      const newScore = score + oldScore;
      const newPosition = await changePosition(
          oldScore, newScore, gameId, userId,
      );

      gameDataStats.position = newPosition;
      gameDataStats.allRounds += data.rounds;
      gameDataStats.winRounds += data.winRounds;
      gameDataStats.score += score;
      gameDataStats.duration += data.duration;

      batch.set(ref, gameDataStats);
      const result = await batch.commit();
      if (result) {
        return {
          user: gameDataStats,
        };
      }
    }
  } catch (err) {
    return {
      response: "error",
      message: err.message,
    };
  }
};

const addBots = async (req) => {
  try {
    const db = admin.firestore();
    const dateNow = moment.utc();
    const batch = await db.batch();
    const {gameId, iterations} = req;
    const globalUserArray = [];
    const gameStatsArray = [];
    const gameArray = [];
    const gameCollectionArray = [];

    // create 50 bots, works correctly only with 1 iteration
    let newPosition = 0;
    let newScore = 100000;
    for (let i = 0; i < iterations; i += 1) {
      newPosition = 0 + i*125;
      newScore = 100000 - i*125*8;
      while (gameCollectionArray.length < 125) {
        const playerId = uuidv4();
        const gameIdCheck = gameId && setGame(gameId) ?
          gameId : null;
        if (!gameIdCheck) {
          return {response: "error", message: "invalid gameId"};
        }
        const nickname = uuidv4();
        const gameCollection = {
          nickname: nickname,
          playerId,
          balance: {cash: 0},
          lvl: 0,
        };

        const gameStatsDefault = {
          position: newPosition,
          allRounds: 0,
          winRounds: 0,
          score: newScore,
          duration: 0,
        };
        newPosition += 1;
        newScore -= 8;

        const gameClient = {
          gameId,
          playerId,
          created: dateNow,
          updated: dateNow,
          isOnline: false,
        };
        const randomDevId = uuidv4();
        const newUserId = uuidv4();
        const globalUser = {
          deviceId: randomDevId,
          userId: newUserId,
          createdAt: dateNow,
          updatedAt: dateNow,
        };
        const dataForJWT = {
          playerId: playerId,
          userId: newUserId,
          gameId: gameId,
        };
        const userTokenJWT = createToken(dataForJWT);
        if (!userTokenJWT) {
          return {response: "error", message: "error Token"};
        }
        globalUserArray.push(globalUser);
        gameStatsArray.push({
          playerId,
          userId: newUserId,
          ...gameStatsDefault,
        });
        gameArray.push({userId: newUserId,
          userTokenJWT,
          ...gameClient});
        gameCollectionArray.push(gameCollection);
      }
      botAdder(
          globalUserArray,
          gameStatsArray,
          gameArray,
          gameCollectionArray,
          gameId,
      );
      globalUserArray.length = 0;
      gameStatsArray.length = 0;
      gameArray.length = 0;
      gameCollectionArray.length = 0;
    }
    const result = await batch.commit();
    if (result) {
      return {
        response: "ok",
      };
    } else {
      return {
        response: "error",
        message: "error creating a new account",
      };
    }
  } catch (err) {
    return {
      response: "error",
      message: err.message,
    };
  }
};

const getLeaderboard = async (req) => {
  const {
    usertokenjwt: userTokenJWT,
  } = req.headers;
  const {userId, limit} = req.query;
  const db = admin.firestore();
  const decoded = decipherToken(userTokenJWT);

  //  check JWT
  const currentUser = await db
      .collection(`${collectionName.playerStatistic}_${decoded.gameId}`)
      .doc(decoded.playerId).get();
  if (currentUser.data().userId !== userId) {
    return {
      response: "error",
      message: "error with jwt/userid",
    };
  }

  //  get top
  const statsRef = db
      .collection(`${collectionName.playerStatistic}_${decoded.gameId}`)
      .orderBy("score", "desc");
  const snapshotTop = await statsRef.limit(+limit).get();
  const topPlayers = snapshotTop.docs.map((element) => element.data());

  // get user + neighbours
  let userAndNeighbours;
  if (currentUser.data().position > +limit) {
    const statsRefPosition = db
        .collection(`${collectionName.playerStatistic}_${decoded.gameId}`)
        .orderBy("position", "desc");
    const snapshotUser = await statsRefPosition
        .startAt(currentUser.data().position+1)
        .endAt(currentUser.data().position-1).get();
    userAndNeighbours = snapshotUser.docs.map((element) => element.data());
  } else {
    userAndNeighbours = `you are in top on pos ${currentUser.data().position}`;
  }

  return {
    userAndNeighbours,
    topPlayers,
  };
};

module.exports.createUser = createUser;
module.exports.getUser = getUser;
module.exports.updateStats = updateStats;
module.exports.addBots = addBots;
module.exports.getLeaderboard = getLeaderboard;
